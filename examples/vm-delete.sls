{% set location = 'eastus' %}
{% set rg_name = 't0-idem-test' %}
{% set vnet_name = 't0-idem-test-network' %}
{% set public_ip_name = 't0-idem-test-public-ip' %}
{% set ip_config_name = "t0-idem-test-ip-config" %}
{% set nic_name = 't0-idem-test-nic' %}
{% set subnet_name = 't0-idem-test-subnet' %}
{% set vm_name = 't0-idem-test-vm' %}
{% set vm_user_name = 'ubuntu' %}
{% set vm_user_passwd = 'change@me1' %}
{% set sls_rev = '1.0.1' %}

Assure Virtual Machine Absent:
  azure.compute.virtual_machines.absent:
  - resource_group_name: {{ rg_name }}
  - vm_name: {{ vm_name }}

Assure Network Interface Absent:
  azure.network.network_interfaces.absent:
  - resource_group_name: {{ rg_name }}
  - network_interface_name: {{ nic_name }}

Assure Public IP Address Present:
  azure.network.public_ip_addresses.absent:
  - resource_group_name: {{ rg_name }}
  - public_ip_address_name: {{ public_ip_name }}

Assure Subnet Absent:
  azure.network.subnets.absent:
   - resource_group_name: {{ rg_name }}
   - virtual_network_name: {{ vnet_name }}
   - subnet_name: {{ subnet_name }}

Assure Virtual Network Absent:
  azure.network.virtual_networks.absent:
  - resource_group_name: {{ rg_name }}
  - virtual_network_name: {{ vnet_name }}

Assure Resource Group Absent:
  azure.resource.resource_groups.absent:
  - resource_group_name: {{ rg_name }}
